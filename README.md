AR-atlassian-configuration
=========

Any tasks related to the configuration of confluence and jira go here. Possibly any bitbucket configurations if/when they are developed.

Requirements
------------

Ansible Version : {{ insert value here }}
Boto Version : {{ insert value here }}

Role Variables
--------------

Dependencies
------------

Example Playbook
----------------

License
-------

BSD

Author Information
------------------
